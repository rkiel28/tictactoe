#pragma once



#include <sstream>

#include <string>



class TicTacToe
{

private:



    

    char m_board[9] = {'1', '2', '3', '4', '5', '6', '7', '8', '9' };

    int m_numTurns = 1; 

    char m_playerTurn = 1;

    char m_winner = ' '; // Check winner



public:

    

   


    
    

    void DisplayBoard()
    {
        
        std::cout << "\n\n\tTic Tac Toe\n\n";

        std::cout << "Player 1 (X) - Player 2 (O)" << "\n" << "\n";
        std::cout << "\n";


        std::cout << " " << m_board[0] << " | " << m_board[1] << " | " << m_board[2] << " | " << "\n";

        std::cout << " __|___|___|" << "\n";
        std::cout << "   |   |   |" << "\n";

        std::cout << " " << m_board[3] << " | " << m_board[4] << " | " << m_board[5] << " | " << "\n";

        std::cout << "___|___|___|" << "\n";
        std::cout << "   |   |   |" << "\n";

        std::cout << " " << m_board[6] << " | " << m_board[7] << " | " << m_board[8] << " | " << "\n";

        std::cout << "___|___|___|" << "\n" << "\n";
    }


    bool IsOver()
    {
        if (m_board[0] == m_board[1] && m_board[1] == m_board[2])
        {
            m_winner = m_board[0] == 'X' ? '1' : '2';
            return true; 
        }
        if (m_board[3] == m_board[4] && m_board[4] == m_board[5])
        {
            m_winner = m_board[3] == 'X' ? '1' : '2';
            return true;
        }
        if (m_board[6] == m_board[7] && m_board[7] == m_board[8])
        {
            m_winner = m_board[6] == 'X' ? '1' : '2';
            return true;
        }
        if (m_board[0] == m_board[3] && m_board[3] == m_board[6])
        {
            m_winner = m_board[0] == 'X' ? '1' : '2';
            return true;
        }
        if (m_board[1] == m_board[4] && m_board[4] == m_board[7])
        {
            m_winner = m_board[1] == 'X' ? '1' : '2';
            return true;
        }
        if (m_board[2] == m_board[5] && m_board[5] == m_board[8])
        {
            m_winner = m_board[2] == 'X' ? '1' : '2';
            return true;
        }
        if (m_board[0] == m_board[4] && m_board[4] == m_board[8])
        {
            m_winner = m_board[0] == 'X' ? '1' : '2';
            return true;
        }
        if (m_board[2] == m_board[4] && m_board[4] == m_board[6])
        {
            m_winner = m_board[2] == 'X' ? '1' : '2';
            return true;
        }

        //check for tie game
            
        if (m_numTurns == 10)
            return true;
        

        return false;
    }

    char GetPlayerTurn()
    {
        
        
            if (m_numTurns % 2)
                m_playerTurn = '1'; // odd
            else
               m_playerTurn = '2'; // even

            return m_playerTurn;
        
       

        
    }

    bool IsValidMove(int position)
    {
        return !(m_board[position - 1] == 'X' || m_board[position - 1] == 'O');
    }

    void Move(int position) 
    {
        char player = 'O';


        if (m_numTurns % 2)
            player = 'X'; 

        m_board[position - 1] = player;

        m_numTurns++;
       
         
        


    }

    void DisplayResult()
    {
        if (m_winner != ' ')

            std::cout << "==>\a Player " << m_winner << " is the winner!!!";
        else
            std::cout << "==>\a Tie Game!";
    }





};